%%% dsc_log.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the functions used in logging in
%%% the {@link //dsc. dsc} application.
%%%

-module(dsc_log).

%% export dsc_log public API
-export([traffic_open/0, traffic_close/0]).
-export([traffic_log/6]).
-export([traffic_query/7]).
-export([query_log/5]).

%% export dsc_log private API
-export([traffic_query/5]).

-define(TRAFFICLOG, dsc_traffic).

%% support deprecated_time_unit()
-define(MILLISECOND, milli_seconds).
%-define(MILLISECOND, millisecond).

% calendar:datetime_to_gregorian_seconds({{1970,1,1},{0,0,0}})
-define(EPOCH, 62167219200).

-include("dsc_log.hrl").

%%----------------------------------------------------------------------
%%  The dsc_log public API
%%----------------------------------------------------------------------

-spec traffic_open() -> Result
	when
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Open traffic event disk log.
traffic_open() ->
	{ok, Directory} = application:get_env(dsc, traffic_log_dir),
	{ok, LogSize} = application:get_env(dsc, traffic_log_size),
	{ok, LogFiles} = application:get_env(dsc, traffic_log_files),
	open_log(Directory, ?TRAFFICLOG, LogSize, LogFiles).

-spec traffic_log(Service, OrigPeer, DestPeer, Request, Answer,
		ResultCode) -> Result
	when
		Service :: diameter:service_name(),
		OrigPeer :: undefined | string() | binary(),
		DestPeer :: undefined | string() | binary(),
		Request :: undefined | diameter_codec:message(),
		Answer :: undefined | diameter_codec:packet(),
		ResultCode :: undefined | integer(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Write a DIAMETER traffic event to traffic log.
traffic_log(Service, OrigPeer, DestPeer, Request, Answer, ResultCode) ->
	Event = [node(), Service, OrigPeer, DestPeer, Request, Answer, ResultCode],
	write_log(?TRAFFICLOG, Event).

-spec traffic_close() -> Result
	when
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc Close traffice event disk log.
traffic_close() ->
	close_log(?TRAFFICLOG).

-spec open_log(Directory, Log, LogSize, LogFiles) -> Result
	when
		Directory  :: string(),
		Log :: atom(),
		LogSize :: integer(),
		LogFiles :: integer(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc open disk log file
open_log(Directory, Log, LogSize, LogFiles) ->
	case file:make_dir(Directory) of
		ok ->
			open_log1(Directory, Log, LogSize, LogFiles);
		{error, eexist} ->
			open_log1(Directory, Log, LogSize, LogFiles);
		{error, Reason} ->
			{error, Reason}
	end.
%% @hidden
open_log1(Directory, Log, LogSize, LogFiles) ->
	FileName = Directory ++ "/" ++ atom_to_list(Log),
	case disk_log:open([{name, Log}, {file, FileName},
					{type, wrap}, {size, {LogSize, LogFiles}}]) of
		{ok, Log} ->
			ok;
		{repaired, Log, _Recovered, _Bad} ->
			ok;
		{error, Reason} ->
			Descr = lists:flatten(disk_log:format_error(Reason)),
			Trunc = lists:sublist(Descr, length(Descr) - 1),
			error_logger:error_report([Trunc, {module, ?MODULE},
					{log, Log}, {error, Reason}]),
			{error, Reason}
	end.

-spec write_log(Log, Event) -> Result
	when
		Log :: atom(),
		Event :: list(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc write event into given log file
write_log(Log, Event) ->
	TS = erlang:system_time(?MILLISECOND),
	N = erlang:unique_integer([positive]),
	LogEvent = list_to_tuple([TS, N | Event]),
	disk_log:log(Log, LogEvent).

-spec close_log(Log) -> Result
	when
		Log :: atom(),
		Result :: ok | {error, Reason},
		Reason :: term().
%% @doc close log files
close_log(Log) ->
	case disk_log:close(Log) of
		ok ->
			ok;
		{error, Reason} ->
			Descr = lists:flatten(disk_log:format_error(Reason)),
			Trunc = lists:sublist(Descr, length(Descr) - 1),
			error_logger:error_report([Trunc, {module, ?MODULE},
					{log, Log}, {error, Reason}]),
			{error, Reason}
	end.

-spec traffic_query(Continuation, Start, End, OriginHost, DestinationHost,
		ResultCode, AttrsMatch) -> Result
	when
		Continuation :: start | disk_log:continuation(),
		Start :: calendar:datetime() | pos_integer(),
		End :: calendar:datetime() | pos_integer(),
		OriginHost :: string() | binary() | '_',
		DestinationHost :: string() | binary() | '_',
		ResultCode :: integer() | '_',
		AttrsMatch :: [{Attribute, Match}] | '_',
		Attribute :: byte(),
		Match :: {exact, term()} | {notexact, term()}
				| {lt, term()} | {lte, term()}
				| {gt, term()} | {gte, term()}
				| {regex, term()} | {like, [term()]} | {notlike, [term()]}
				| {in, [term()]} | {notin, [term()]} | {contains, [term()]}
				| {notcontain, [term()]} | {containsall, [term()]} | '_',
		Result :: {Continuation2, Events} | {error, Reason},
		Continuation2 :: eof | disk_log:continuation(),
		Events :: [traffic_event()],
		Reason :: term().
%% @doc Query traffic log events with filters.
%%
%% 	The first time called `Continuation' should have the value `start'.
%%
%% 	Events before `Start' or after `Stop', or which do not match
%% 	the `OriginHost', `DestinationHost' or `ResultCode', are ignored.
%%
%% 	Events which do not include `Attribute' in attributes are ignored.
%% 	If `Match' is '_' any attribute value will match or
%% 	`{Operator, MatchValue}' may be used for more complex queries.
%%
%% 	All attribute filters must match or the event will be ignored.
%%
%% 	`OriginHost', `DestinationHost', `ResultCode', or `AttrsMatch' may be '_' which matches any value.
%%
%% 	Returns a new `Continuation' and a list of matching traffic events.
%% 	Successive calls use the new `Continuation' to read more events.
%%
traffic_query(Continuation, Start, End, OriginHost, DestinationHost,
		ResultCode, AttrsMatch) ->
	MFA = {?MODULE, traffic_query, [OriginHost, DestinationHost, ResultCode, AttrsMatch]},
	query_log(Continuation, Start, End, ?TRAFFICLOG, MFA).


-spec query_log(Continuation, Start, End, Log, MFA) -> Result
	when
		Continuation :: start | disk_log:continuation(),
		Start :: calendar:datetime() | pos_integer(),
		End :: calendar:datetime() | pos_integer(),
		MFA :: {Module, Function, Args},
		Log :: atom(),
		Module :: atom(),
		Function :: atom(),
		Args :: [Arg],
		Arg :: term(),
		Result :: {Continuation2, Events} | {error, Reason},
		Continuation2 :: eof | disk_log:continuation(),
		Events :: [term()],
		Reason :: term().
%% @doc
query_log(Continuation, {{_, _, _}, {_, _, _}} = Start, End, Log, MFA) ->
	Seconds = calendar:datetime_to_gregorian_seconds(Start) - ?EPOCH,
	query_log(Continuation, Seconds * 1000, End, Log, MFA);
query_log(Continuation, Start, {{_, _, _}, {_, _, _}} = End, Log, MFA) ->
	Seconds = calendar:datetime_to_gregorian_seconds(End) - ?EPOCH,
	query_log(Continuation, Start, Seconds * 1000 + 999, Log, MFA);
query_log(start, Start, End, Log, MFA) when is_integer(Start), is_integer(End) ->
	query_log1(Start, End, Log, MFA, [], disk_log:bchunk(Log, start));
query_log(Continuation, Start, End, Log, MFA) when is_integer(Start), is_integer(End) ->
	query_log2(Start, End, MFA, disk_log:chunk(Log, Continuation), []).
%% @hidden
query_log1(Start, End, _Log, MFA, PrevChunk, eof) ->
	Chunk = [binary_to_term(E) || E <- PrevChunk],
	query_log2(Start, End, MFA, {eof, Chunk}, []);
query_log1(_Start, _End, _Log, _MFA, _PrevChunk, {error, Reason}) ->
	{error, Reason};
query_log1(Start, End, Log, MFA, PrevChunk, {Cont, [H | T] = Chunk}) ->
	case binary_to_term(H) of
		Event when element(1, Event) > End ->
			{eof, []};
		Event when element(1, Event) >= Start ->
			NewChunk = [binary_to_term(E) || E <- PrevChunk ++ Chunk],
			query_log2(Start, End, MFA, {Cont, NewChunk}, []);
		_Event ->
			query_log1(Start, End, Log, MFA, T, disk_log:bchunk(Log, Cont))
	end.
%% @hidden
query_log2(_Start, _End, {M, F, A}, eof, Acc) ->
	apply(M, F, [{eof, lists:reverse(Acc)} | A]);
query_log2(_Start, _End, _MFA, {error, Reason}, _Acc)->
	{error, Reason};
query_log2(_Start, End, {M, F, A}, {_, [Event | _]}, Acc) when element(1, Event) > End ->
	apply(M, F, [{eof, lists:reverse(Acc)} | A]);
query_log2(Start, End, MFA, {Cont, [Event | T]}, Acc)
		when element(1, Event) >= Start, element(1, Event) =< End ->
	query_log2(Start, End, MFA, {Cont, T}, [Event | Acc]);
query_log2(Start, End, MFA, {Cont, [_ | T]}, Acc) ->
	query_log2(Start, End, MFA, {Cont, T}, Acc);
query_log2(_Start, _End, {M, F, A}, {Cont, []}, Acc) ->
	apply(M, F, [{Cont, lists:reverse(Acc)} | A]).

-spec traffic_query(Continuation, OrigHost, DestHost, ResultCode, MatchSpec) -> Result
	when
		Continuation :: {Continuation2, Events},
		OrigHost :: string() | binary() | '_',
		DestHost :: string() | binary() | '_',
		ResultCode :: integer() | '_',
		MatchSpec :: [tuple()] | '_',
		Result :: {Continuation2, Events},
		Continuation2 :: eof | disk_log:continuation(),
		Events :: [traffic_event()].
%% @private
%% @doc Query accounting log events with filters.
%%
traffic_query({Cont, Events}, OrigHost, DestHost, ResultCode, AttrsMatch) ->
	{Cont, traffic_query1(Events, OrigHost, DestHost, ResultCode, AttrsMatch, [])}.

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

%% @hidden
traffic_query1(Events, '_', DH, RCode, AMatch, Acc) ->
	traffic_query2(Events, DH, RCode, AMatch, Acc);	
traffic_query1([H | T], OH, DH, RCode, AMatch, Acc) ->
	case element(5, H) of
		OH ->
			traffic_query1(T, OH, DH, RCode, AMatch, [H | Acc]);
		_ ->
			traffic_query1(T, OH, DH, RCode, AMatch, Acc)
	end;
traffic_query1([], _OH, DH, RCode, AMatch, Acc) ->
	traffic_query2(lists:reverse(Acc), DH, RCode, AMatch, []).

%% @hidden
traffic_query2(Events, '_', RCode, AMatch, Acc) ->
	traffic_query3(Events, RCode, AMatch, Acc);
traffic_query2([H | T], DH, RCode, AMatch, Acc) ->
	case element(6, H) of
		DH ->
			traffic_query2(T, DH, RCode, AMatch, [H | Acc]);
		_ ->
			traffic_query2(T, DH, RCode, AMatch, Acc)
	end;
traffic_query2([], _DH, RCode, AMatch, Acc) ->
	traffic_query3(lists:reverse(Acc), RCode, AMatch, []).

traffic_query3(Events, '_', AMatch, Acc) ->
	traffic_query4(Events, AMatch, Acc);
traffic_query3([H | T], RCode, AMatch, Acc) ->
	case element(9, H) of
		RCode ->
			traffic_query3(T, RCode, AMatch, [H | Acc]);
		_ ->
			traffic_query3(T, RCode, AMatch, Acc)
	end;
traffic_query3([], _RCode, AMatch, Acc) ->
	traffic_query4(lists:reverse(Acc), AMatch, []).

%% @hidden
traffic_query4(Events, _AMatch, _Acc) ->
	Events.

