%%% dsc.erl
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @copyright 2015-2018 SigScale Global Inc.
%%% @end
%%% Licensed under the Apache License, Version 2.0 (the "License");
%%% you may not use this file except in compliance with the License.
%%% You may obtain a copy of the License at
%%%
%%%     http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing, software
%%% distributed under the License is distributed on an "AS IS" BASIS,
%%% WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
%%% See the License for the specific language governing permissions and
%%% limitations under the License.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% @doc This library module implements the public API for the
%%% 	{@link //dsc. dsc} application.
%%%

-module(dsc).

%% export the dsc public API
-export([add_service/5, find_service/1, delete_service/1, start_service/1,
		add_peer/7, add_peer/8, find_peer/1, delete_peer/1, start_peer/1,
		add_realm/4, find_realm/1, delete_realm/1]).

%% export the dsc public API
-export([start_service/3, start_peer/5]).

-include_lib("diameter/include/diameter.hrl").
-include("dsc.hrl").

-spec add_service(Name, LAddress, LPort, Options, Enabled) -> Result
	when
		Name :: term(),
		LAddress :: inet:ip_address(),
		LPort :: inet:port_number(),
		Options :: [diameter:transport_opt()| diameter:service_opt()],
		Enabled :: boolean(),
		Result :: {ok, #dsc_service{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_service' table.
%%	`transport' in `Options' defines the listening transport of the relay service.
%%	Defaults to sctp.
%%
add_service(Name, LAddress, LPort, Options, Enabled) when is_list(LAddress),
		is_list(Options), is_boolean(Enabled) ->
	case inet:parse_address(LAddress) of
		{ok, LAddr} ->
			add_service(Name, LAddr, LPort, Options, Enabled);
		{error, Reason} ->
			{error, Reason}
	end;
add_service(Name, LAddress, LPort, Options, Enabled) when is_tuple(LAddress),
		is_list(Options), is_boolean(Enabled) ->
	F = fun() ->
				R = #dsc_service{name = Name, laddress = LAddress, lport = LPort,
						enabled = Enabled, options = Options},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Service} ->
			{ok, Service};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_service(Name) -> Result
	when
		Name :: term(),
		Result :: {ok, #dsc_service{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a service  by name.
%%
find_service(Name) ->
	F = fun() ->
				mnesia:read(dsc_service, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_service{} = Service]} ->
			{ok, Service};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_service(Name) -> ok
	when
		Name :: term().
%% @doc Delete an entry from the `dsc_service' table.
delete_service(Name)  ->
	F = fun() ->
		mnesia:delete(dsc_service, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options) -> Result
	when
		Name :: string() | binary() ,
		Svc :: diameter:service_name(),
		LAddress :: inet:ip_address(),
		LPort :: inet:port_number(),
		RAddress :: inet:ip_address(),
		RPort :: inet:port_number(),
		Options :: [diameter:transport_opt()],
		Result :: {ok, #dsc_peer{}} | {error, Reason},
		Reason :: term().
%% @equiv add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options, true)
%%
add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options)  ->
 add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options, true).

-spec add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options, Active) -> Result
	when
		Name :: string() | binary() ,
		Svc :: diameter:service_name(),
		LAddress :: inet:ip_address(),
		LPort :: inet:port_number(),
		RAddress :: inet:ip_address(),
		RPort :: inet:port_number(),
		Options :: [diameter:transport_opt()],
		Active :: boolean(),
		Result :: {ok, #dsc_peer{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_peer' table.
%%
add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options, Active) when is_list(Name),
		is_boolean(Active), is_list(Options) ->
	add_peer(list_to_binary(Name), Svc, LAddress, LPort, RAddress, RPort, Options, Active);
add_peer(Name, Svc, LAddress, LPort, RAddress, RPort, Options, Active) when is_binary(Name),
		is_boolean(Active), is_list(Options) ->
	F = fun() ->
				R = #dsc_peer{name= Name, service = Svc, laddress = LAddress,
						lport = LPort, raddress = RAddress, rport = RPort, options = Options,
						active = Active},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Peer} ->
			{ok, Peer};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_peer(Name) -> Result
	when
		Name :: string() | binary(),
		Result :: {ok, #dsc_peer{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a peer by identity.
%%
find_peer(Name) when is_list(Name) ->
	find_peer(list_to_binary(Name));
find_peer(Name) when is_binary(Name) ->
	F = fun() ->
				mnesia:read(dsc_peer, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_peer{} = Peer]} ->
			{ok, Peer};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_peer(Name) -> ok
	when
		Name :: string() | binary().
%% @doc Delete an entry from the `dsc_peer' table.
delete_peer(Name) when is_list(Name) ->
	delete_peer(list_to_binary(Name));
delete_peer(Name) when is_binary(Name) ->
	F = fun() ->
		mnesia:delete(dsc_peer, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.


-spec add_realm(Name, ApplicationId, Mode, Peers) -> Result
	when
		Name :: string() | binary(),
		ApplicationId :: non_neg_integer(),
		Mode :: local | relay | proxy | redirect,
		Peers :: [PeerName],
		PeerName :: string(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: term().
%% @doc Create an entry in `dsc_realm' table.
%%
add_realm(Name, AppId, Mode, Peers) when is_list(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
add_realm(list_to_binary(Name), AppId, Mode, Peers);
add_realm(Name, AppId, Mode, Peers) when is_binary(Name),
		is_integer(AppId), is_atom(Mode), is_list(Peers) ->
	F = fun() ->
				R = #dsc_realm{name = Name, app_id = AppId, mode = Mode,
						peers = Peers},
				ok = mnesia:write(R),
				R
	end,
	case mnesia:transaction(F) of
		{atomic, Realm} ->
			{ok, Realm};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec find_realm(Name) -> Result
	when
		Name :: string() | binary(),
		Result :: {ok, #dsc_realm{}} | {error, Reason},
		Reason :: not_found | term().
%% @doc Find a realm by name
%%
find_realm(Name) when is_list(Name) ->
	find_realm(list_to_binary(Name));
find_realm(Name) when is_binary(Name) ->
	F = fun() ->
				mnesia:read(dsc_realm, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_realm{} = Realm]} ->
			{ok, Realm};
		{atomic, []} ->
			{error, not_found};
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec delete_realm(Name) -> ok
	when
		Name :: string() | binary().
%% @doc Delete an entry from the `dsc_realm' table.
delete_realm(Name) when is_list(Name) ->
	delete_realm(list_to_binary(Name));
delete_realm(Name) when is_binary(Name) ->
	F = fun() ->
		mnesia:delete(dsc_realm, Name, write)
	end,
	case mnesia:transaction(F) of
		{atomic, _} ->
			ok;
		{aborted, Reason} ->
			exit(Reason)
	end.

-spec start_service(Name) -> Result
	when
		Name :: term(),
		Result :: {ok, pid()} | {error, Reason},
		Reason :: term().
%% @doc Start a DIAMETER relay service fsm supervisor for provided `Name'
%% in the `dsc_service' table.
start_service(Name)  ->
	F = fun() ->
		mnesia:read(dsc_service, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_service{laddress = Address, lport = Port, options = Options}]} ->
			start_service(Address, Port, Options);
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec start_service(Address, Port, Options) -> Result
	when
		Address :: inet:ip_address(),
		Port :: inet:port_number(),
		Options :: [diameter:transport_opt() | diameter:service_opt()],
		Result :: {ok, Pid} | {error, Reason},
		Pid :: pid(),
		Reason :: term().
%% @doc Start a DIAMETER relay service fsm supervisor.
%%	`transport' in `Options' defines the listening transport of the relay service.
%%	Defaults to sctp.
%% @private
start_service(Address, Port, Options) when is_tuple(Address), is_number(Port),
		is_list(Options) ->
	gen_server:call(dsc, {start, Address, Port, Options}).

-spec start_peer(Name) -> Result
	when
		Name :: string() | binary(),
		Result :: {ok, TrasnportReference} | {error, Reason},
		TrasnportReference :: term(),
		Reason :: term().
%% @doc Start a connection to a remote DIAMETER peer for provided `Name'
%% in the `dsc_peer' table.
start_peer(Name) when is_list(Name) ->
	start_peer(list_to_binary(Name));
start_peer(Name) when is_binary(Name) ->
	F = fun() ->
			mnesia:read(dsc_peer, Name, read)
	end,
	case mnesia:transaction(F) of
		{atomic, [#dsc_peer{service = Svc, laddress = LAddress,
				raddress = RAddress, rport = RPort, options = Options}]} ->
			start_peer(Svc, LAddress, RAddress, RPort, Options);
		{aborted, Reason} ->
			{error, Reason}
	end.

-spec start_peer(Svc, LAddress, RAddress, RPort, Options) -> Result
	when
		Svc :: diameter:service_name(), 
		LAddress :: inet:ip_address(),
		RAddress :: inet:ip_address(),
		RPort :: inet:port_number(),
		Options :: [diameter:transport_opt()],
		Result :: {ok, TrasnportReference} | {error, Reason},
		TrasnportReference :: term(),
		Reason :: term().
%% @doc Start a connection to a remote DIAMETER peer at `Address', `Port'
%% with `Options'.
%% @private
start_peer(Svc, LAddress, RAddress, RPort, Options) ->
	Apps = application:which_applications(),
	case lists:keyfind(diameter, 1, Apps) of
		{_, _, _} ->
			start_peer1(Svc, diameter:services(), LAddress, RAddress, RPort, Options);
		false ->
			{error, diameter_not_started}
	end.

%% @hidden
start_peer1(_, [], _, _, _, _) ->
	{error,  diameter_service_not_started};
start_peer1(Svc, [Svc | _], LAddress, RAddress, RPort, Options) ->
		start_peer2(Svc, LAddress, RAddress, RPort, Options);
start_peer1(Svc, [_ | T], LAddress, RAddress, RPort, Options) ->
	start_peer1(Svc, T, LAddress, RAddress, RPort, Options).

%% @hidden
start_peer2(Svc, LAddress, RAddress, RPort, Options) ->
	TConfig = transport_config(LAddress, RAddress, RPort, Options),
	case diameter:add_transport(Svc, {connect, TConfig}) of
		{ok, Ref} ->
			{ok, Ref};
		{error, Reason} ->
			{error, Reason}
	end.

%%---------------------------------------------------------------------
%%  Internal functions
%%---------------------------------------------------------------------

-spec transport_config(LocalAddr, RemAddr, RemPort, Options) -> NewOptions
	when
		LocalAddr :: inet:ip_address(),
		RemAddr :: inet:ip_address(),
		RemPort :: inet:port_number(),
		Options :: [diameter:transport_opt()],
		NewOptions :: [diameter:transport_opt()].
%% @doc Returns options for a DIAMETER transport layer
%% @hidden
transport_config(LocalAddr, RemAddr, RemPort, Options) ->
	DefaultOpts = [{transport_module, diameter_sctp},
		{transport_config, [{raddr, RemAddr},
				{rport, RemPort},
				{reuseaddr, true},
				{ip, LocalAddr}]}],
	F = fun(F, [{K, _} = H | T], O) ->
			case lists:keymember(K, 1, O) of
				true ->
					F(F, T, lists:keyreplace(K, 1, O, H));
				false ->
					F(F, T, O)
			end;
		(_, [], O) ->
			O
	end,
	F(F, Options, DefaultOpts).

